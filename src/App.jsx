import { useState } from 'react'
import './App.css'
import { useEffect } from 'react'
import axios from 'axios'

function App() {
  const [city, setCity] = useState('Kochi')
  const [temperature, setTemperature] = useState(0)
  const [searchResults, setSearchResults] = useState([])

  useEffect(() => {
    let longitude;
    let latitude;
    if (city === 'Kochi') {
      latitude = 9.9312
      longitude = 76.2673
    }
    else if (city === 'Kozhikode') {
      latitude = 11.2588
      longitude = 75.7804
    }
    else if (city === 'Trivandrum') {
      latitude = 8.5241
      longitude = 76.9366
    }
    else {
      latitude = 13.0827
      longitude = 80.2707
    }

    axios.get(`https://api.open-meteo.com/v1/forecast?latitude=${latitude}&longitude=${longitude}&current=temperature_2m,wind_speed_10m&hourly=temperature_2m,relative_humidity_2m,wind_speed_10m`)
      .then(res => setTemperature(res.data.current.temperature_2m))
      .catch(error => console.log(error))
  }, [city])

  function handleSearch(e) {
    e.preventDefault()
    const form = e.target
    const searchKeyword = form['search'].value

    const apiUrl = `https://geocoding-api.open-meteo.com/v1/search?name=${searchKeyword}&count=10&language=en&format=json`
    axios.get(apiUrl)
      .then(res => setSearchResults(res.data.results))
      .catch(error => console.log(error))
  }

  return (
    <>
      <header className='h-16 py-4 bg-green-800'>
        <div className='container mx-auto px-4 text-center'><span className='text-lg font-bold text-white'>WeatherNow</span></div>
      </header>
      <main>
        <section className='py-16 flex flex-col items-center justify-center'>
          <h1 className='text-3xl'>Current weather at {city} is <span className='text-5xl text-orange-500 font-bold'>{temperature}</span>&deg;C</h1>
          <div className='mt-8 flex flex-row gap-4'>
            <button onClick={() => setCity('Kochi')} className='h-10 px-6 border-green-800 border rounded text-green-800 font-semibold'>Kochi</button>
            <button onClick={() => setCity('Kozhikode')} className='h-10 px-6 border-green-800 border rounded text-green-800 font-semibold'>Kozhikode</button>
            <button onClick={() => setCity('Trivandrum')} className='h-10 px-6 border-green-800 border rounded text-green-800 font-semibold'>Trivandrum</button>
            <button onClick={() => setCity('Chennai')} className='h-10 px-6 border-green-800 border rounded text-green-800 font-semibold'>Chennai</button>
          </div>
          <form onSubmit={handleSearch} className='mt-16 flex flex-col items-center w-[70%]'>
            <label htmlFor="search">Search city:</label>
            <div className='flex flex-row items-center gap-4 mt-6 '>
              <input className='w-full h-10 p-2 border border-indigo-500' type="text" id='search' />
              <button className='h-10 bg-orange-600 px-6 rounded text-white font-semibold'>Search</button>
            </div>
          </form>
          <ul>
            {
              searchResults.map((result, index) => {
                return (
                  <li className='p-2 flex flex-row gap-6 border rounded' key={index}><h3>{result.name}</h3><span>{result.country}</span><span>{result.admin2}</span></li>
                )
              })
            }
          </ul>
        </section>
      </main>
      <footer></footer>
    </>
  )
}

export default App
